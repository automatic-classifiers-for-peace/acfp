# Template experiment

## Meta data
Authors:
- Benjamin Cerigo: benjamin@datavaluepeople.com

Git tag when completed: this experiment will not be depreciated and should always work on the
latest commit of main. Non dummy experiments should update this with the git tag when the
completed experiment was merged to `main`.

## Introduction and context
This experiment is a dummy experiment as a starting point for future experiments.

## Set up
Make sure your python version matches `acfp/.python-version` (root project). It is recommended to
use [`pyenv`](https://github.com/pyenv/pyenv#installation) to manage python versions.

Create a virtual environment and set it up:
```
python -m venv .venv
source .venv/bin/activate
make install
```

## Running the experiment
`python main.py`

## Results, Findings, Limitations and Conclusions
The template experiment works if you see "Template experiment"
