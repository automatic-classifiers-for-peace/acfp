# Zero shot with OpenAI LLM
Large language models have been shown to be effective in classification. This experiments tests if
[OpenAI GPT models](https://platform.openai.com/docs/models/overview) like GPT4 or GPT3.5 can be
used for the classification’s that we are interested in.

## Methodology:
- Annotate random examples until we get 50 positive examples for a class to create a training set
- Design around 4 prompts for the classification
- Run each prompt for the full training set
- Use the F1 accuracy score on the training set to indicate which prompt is the best
- Use the prompt that preformed best to classify the rest of the dataset
- Do this with the models GPT3.5 or GPT4

## Creating the training set
A training set of around 50 positive examples needs to be created for each classification.

To do this:
- Add all the examples in the validation set to a google worksheet and randomly ordered
- Have the annotator look for positive examples. They can do this by searching for words or looking
  through the examples.
- Have the annotator add some random negative examples
- Copy each chosen positive or negative example should be copied to a second google sheet

## Creating the prompts
1. Design around 2 prompts considering some best practices such as
[blog](https://contenthacker.com/chatgpt-prompts/)
2. Run each prompt for the full training set
3. Make two variations on the best preforming prompt and run these on the training set
4. Decided which is the best prompt
