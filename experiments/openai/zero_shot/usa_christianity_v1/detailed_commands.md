# Detailed commands of how to run the experiment
These bash commands document the exact commands that have been run for the experiment.

## Rate limits
Be aware that there are rate limits so you may need to run the classification in batches using
something like:
```
--sleep_between_batches 3
```
If there is an error then use `--start_offset <next_row_to_process>` to restart the classification.

The actual sleep seconds that should be used is depended on the tier you have in the API. For this
experiment the given sleep values were used and the API limits where tier 1.

### General
This command creates the correct variables:
```
CLASSIFICATION=../../acfpopenai/classification.py
PARSE=../../acfpopenai/parsing.py
TS=prompt_training_text_snippets.csv
RPMO=raw_prompt_training_model_output
PPMO=parsed_prompt_training_model_output
RVMO=raw_validation_model_output
PVMO=parsed_validation_model_output
```

### Prompt training RUN
#### process training_set_annotations_BU_HP-2023-11-30.csv
python process_sample_set.py

#### gpt-3
python $CLASSIFICATION $TS prompts/query_1_short_v1.txt $RPMO/gpt3.5_query_1.csv
python $CLASSIFICATION $TS prompts/query_2_detailed_v1.txt $RPMO/gpt3.5_query_2.csv
python $CLASSIFICATION $TS prompts/query_3_gpt_v1.txt $RPMO/gpt3.5_query_3.csv

#### gpt-4
python $CLASSIFICATION $TS prompts/query_1_short_v1.txt $RPMO/gpt4_query_1.csv --model_id gpt-4 --sleep_between_batches 3
python $CLASSIFICATION $TS prompts/query_2_detailed_v1.txt $RPMO/gpt4_query_2.csv --model_id gpt-4 --sleep_between_batches 3
python $CLASSIFICATION $TS prompts/query_3_gpt_v1.txt $RPMO/gpt4_query_3.csv --model_id gpt-4 --sleep_between_batches 3

#### gpt-4-1106-preview
python $CLASSIFICATION $TS prompts/query_1_short_v1.txt $RPMO/gpt4-preview_query_1.csv --model_id gpt-4-1106-preview --sleep_between_batches 1
python $CLASSIFICATION $TS prompts/query_2_detailed_v1.txt $RPMO/gpt4-preview_query_2.csv --model_id gpt-4-1106-preview --sleep_between_batches 1
python $CLASSIFICATION $TS prompts/query_3_gpt_v1.txt $RPMO/gpt4-preview_query_3.csv --model_id gpt-4-1106-preview --sleep_between_batches 1

#### Parse
```
cd $RPMO
for f in *.csv; do python ../$PARSE ${f} is_attitude_polarization_accept is_attitude_polarization_reject ../$PPMO/${f}; done;
cd ..
```

#### Analyse
```
python prompt_training_analyse.py
```

### Validation Run
#### process validation set
```
python process_validation_set.py
```

#### Classification
```
python $CLASSIFICATION validation_text_snippets.csv prompts/query_2_detailed_v1.txt raw_validation_model_output/gpt4_query_2.csv --model_id gpt-4 --self_between_batches 3
python $CLASSIFICATION validation_text_snippets.csv prompts/query_1_short_v1.txt raw_validation_model_output/gpt4-preview_query_1.csv --model_id gpt-4-1106-preview --sleep_between_batches 1
python $CLASSIFICATION validation_text_snippets.csv prompts/query_2_detailed_v1.txt raw_validation_model_output/gpt3.5_query_2.csv --model_id gpt-3.5-turbo --sleep_between_batches 1
```

#### Parse
```
cd $RVMO
for f in *.csv; do python ../$PARSE ${f} is_attitude_polarization_accept is_attitude_polarization_reject ../$PVMO/${f}; done;
cd ..
```

#### Analyse
```
python validation_analyse.py
```
