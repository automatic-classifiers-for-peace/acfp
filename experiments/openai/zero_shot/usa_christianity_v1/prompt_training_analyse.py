"""Analyse the models for each prompt."""
import pandas as pd
import glob

from acfpopenai import metrics

import logging


def main():
    """Analyse the models."""
    folder_with_predictions = "./parsed_prompt_training_model_output/*.csv"
    files = glob.glob(folder_with_predictions)
    logging.info("Found %s files", len(files))
    dfs = [pd.read_csv(filename) for filename in files]
    df = pd.concat(dfs, axis=0)
    df = df.set_index("text_snippet_id")
    labels = pd.read_csv("prompt_training_classification_labels.csv")
    labels = labels.set_index("text_snippet_id")
    result = metrics.binary_f1_score(
        "is_attitude_polarization_accept", "BU_HP", labels, df
    )
    result = result.sort_values(by="f1_score", ascending=False)
    result.to_csv("prompt_training_classification_metrics.csv")
    logging.info(result.to_markdown())


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
