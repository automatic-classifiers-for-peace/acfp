"""Process the sample set to create prompt_training_classification_labels."""
import datetime

import pandas as pd


def main(filename):
    """Process the sample set to create prompt_training_classification_labels."""
    df = pd.read_csv(filename)
    print(df.shape)
    df = df[~df["class_name"].isnull()]
    text_snippets = df[["id", "text", "platform"]]
    classification_labels = df[["id", "class_name"]]
    classification_labels["labeled_by"] = "BU_HP"
    classification_labels["labeled_at"] = datetime.datetime(2020, 11, 30)
    classification_labels = classification_labels.rename(
        columns={"id": "text_snippet_id"}
    )
    classification_labels.set_index("text_snippet_id", inplace=True)
    text_snippets.set_index("id", inplace=True)
    print(classification_labels.index.duplicated().sum())
    print(classification_labels.shape)
    print(text_snippets.shape)
    classification_labels.to_csv("prompt_training_classification_labels.csv")
    text_snippets.to_csv("prompt_training_text_snippets.csv")


if __name__ == "__main__":
    main("training_set_annotations_BU_HP-2023-11-30.csv")
