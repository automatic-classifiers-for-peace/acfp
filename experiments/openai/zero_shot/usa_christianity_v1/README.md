# OpenAI Usa Christianity experiment v1

## Meta data
Authors:
- Benjamin Cerigo: benjamin@datavaluepeople.com

Git tag when completed: this experiment will not be depreciated and should always work on the
latest commit of main. Non dummy experiments should update this with the git tag when the
completed experiment was merged to `main`.

## Introduction and context
This experiment uses the OpenAI Zero shot methodology with the shared USA Christianity data.
- Methodology defined [here](../README.md)
- Validation dataset [here](../../../../shared_data/buildup/usa_christianity/)

## Set up
Make sure your python version matches `acfp/.python-version` (root project). It is recommended to
use [`pyenv`](https://github.com/pyenv/pyenv#installation) to manage python versions.

Get access to the shared data, see link to Validation dataset.

Follow the "OpenAI library and running experiment script section" in [/experiments/openai/](../../)

Change the current directory using `cd zero_shot/usa_christianity_v1/` (if you are in
/experiments/openai)

## Sensitive data
Currently all the sensitive text data is stored in behind a remote authenticated bucket using dvc.
You will need to have access to the buildup shared data. See
[/shared_data/buildup/README.md](/shared_data/buildup/README.md)

If you want to re-run the experiment you will need to pull this data:
- cd to this directory
- Optional is you are using SSO profile: `acfp`
  - `aws sso login --profile acfp`
  - `dvc remote modify --local acfp-buildup-openai-usa-christianity-v1 profile acfp`
- `dvc pull`

Don't forget to the delete the data that was downloaded locally when you have finished with it.

## Commands that where run
See ./detailed_commands.md.

## Methodology
### Creation/annotation of prompt training set
The annotation was done in around 2 hours and by Helena from Build Up using a google sheet
that contained all of the text snippets from the validation set. She was asked to label examples
with either: `is_attitude_polarization_accept` or `is_attitude_polarization_reject`.

She did this by:
- Searching for a word that she thought was polarizing
- Labelling the examples take include this word

In less then 2 hours she was able to label 118 examples to create the prompt training set.

The full export from google work sheet is the file `training_set_annotations_BU_HP-2023-11-30.csv`.
This file is committed to dvc see above and see detailed_commands.md for how this was processed.

### Prompts
We developed 3 prompts:
- short
- detailed
- asked Chatgpt 4 to design a prompts for classifying polarization

See ./prompts/ for actual prompts.

### Classification
#### Technical way of classifying
For the classification [`langchain`](https://python.langchain.com/docs/get_started/introduction)
was used and a "chain" that is defined in the acfpopenai lib:
`experiments/openai/acfpopenai/chains.py::general_classify_function_chain`

This chain:
- Uses a [function call](https://platform.openai.com/docs/guides/function-calling) that is defined in
experiments/openai/acfpopenai/chains.py::general_classify_function This function call then
returns if it is the class that has been described and the certainty float.
- Has the system message "You are a helpful assistant."
- Has the message: `{prompt} {text_snippet}`
- The raw response is saved in a csv file
- The chain is called for each text_snippet

After the raw response for each text_snippet are parsed to create a csv that matches
`classification_labels` schema (in the acfp lib).

See detailed_commands for more information.

#### Prompt training
For the prompt training we ran each prompt for all the text_snippets in the prompt training set for
3 models:
- [gpt-4](https://platform.openai.com/docs/models/gpt-4-and-gpt-4-turbo)
- [gpt-4-1106-preview](https://platform.openai.com/docs/models/gpt-4-and-gpt-4-turbo)
- [gpt-3.5-turbo](https://platform.openai.com/docs/models/gpt-3-5)

These models where chosen to compare gpt-4 with gpt-3.5. And to compare gpt-4-1106-preview with
gpt-4 with the preview being half the price of gpt-4.

Here are the results using [f1
score](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html) from sklearn.
|    | model_id                                                                      |   f1_score |   coverage |
|---:|:------------------------------------------------------------------------------|-----------:|-----------:|
|  6 | gpt-4-1106-preview-zero_shot/usa_christianity/prompts/query_1_short_v1.txt    |   0.75     |    99.1525 |
|  1 | gpt-4-zero_shot/usa_christianity/prompts/query_2_detailed_v1.txt              |   0.728814 |    99.1525 |
|  2 | gpt-4-1106-preview-zero_shot/usa_christianity/prompts/query_3_gpt_v1.txt      |   0.714286 |    99.1525 |
|  3 | gpt-4-1106-preview-zero_shot/usa_christianity/prompts/query_2_detailed_v1.txt |   0.673684 |    99.1525 |
|  5 | gpt-4-zero_shot/usa_christianity/prompts/query_1_short_v1.txt                 |   0.642857 |    99.1525 |
|  4 | gpt-4-zero_shot/usa_christianity/prompts/query_3_gpt_v1.txt                   |   0.60274  |    98.3051 |
|  8 | gpt-3.5-turbo-zero_shot/usa_christianity/prompts/query_2_detailed_v1.txt      |   0.566038 |    99.1525 |
|  0 | gpt-3.5-turbo-zero_shot/usa_christianity/prompts/query_1_short_v1.txt         |   0.559006 |    99.1525 |
|  7 | gpt-3.5-turbo-zero_shot/usa_christianity/prompts/query_3_gpt_v1.txt           |   0.555556 |    99.1525 |

The coverage indicates if the examples in the prompt training set where not classified by the
model. The reason this is not 100 percent is that there was a mistake when running the model and
one or two examples where not included.

In general the gpt-4 models performed reasonable well for the training set. With gpt-4 and detailed
prompt performing the best. gpt-4-preview and the short prompt also preformed well.

Costs to run the training:
- gpt-4: $4.8
- gpt-4-preview: $1.58
- gpt-3.5-turbo: $0.07

The costs reflect the performance but it is noticeable that gpt-4-preview is ~32% the cost for gpt-4
but performs almost as well as gpt-4

### Validation
For the validation we used the best preforming prompt for each model:
- gpt-4-zero_shot/usa_christianity/prompts/query_2_detailed_v1.txt
- gpt-4-1106-preview-zero_shot/usa_christianity/prompts/query_1_short_v1.txt
- gpt-3.5-turbo-zero_shot/usa_christianity/prompts/query_2_detailed_v1.txt

The validation set was made using the full set of annotation in
shared_data/buildup/usa_christianity exclude the ones already used in the training set.

Here are the results using the same analyses as for the prompt training set:
|    | model_id                                        |   f1_score |   coverage |
|---:|:------------------------------------------------|-----------:|-----------:|
|  1 | gpt-4-prompts/query_2_detailed_v1.txt           |   0.461268 |        100 |
|  2 | gpt-4-1106-preview-prompts/query_1_short_v1.txt |   0.446602 |        100 |
|  0 | gpt-3.5-turbo-prompts/query_2_detailed_v1.txt   |   0.214864 |        100 |

It is clear to see that the models preformed poorly for the validation set.

Costs:
- gpt-4: $23.48
- gpt-4-preview: $2.89
- gpt-3.5-turbo: $1.12

It is clear that the cost and performance of gpt-4-preview out weighs the other models.

#### Extra manual research
After looking at some of the text I would like to point out this example:
```
\n\tAsk me how I am feeling: Revelation is the only solution and the Marxists must be put down
to.🙄 But that ship probably sailed. \n
```

For this example (id: 37) all the models predicted true but it was not annotated as true (false
positive). This seems like an interesting example that shows that there may ambiguity when
classifying for polarization. In that a binary classifying of true or false might not encompass
what we are interested. One way of looking at this is that it might be better to classify examples
as a scale (IE. 0-10) to be able to allow for ambiguity.

We could test the ambiguity of the binary classification by having more people annotate examples
and see how much disagreement there is between the annotators. We could then have all annotators
annotate based on a scale of polarization and see if there is less disagreement. It might be
sufficient to have new annotators annotate only false positives and false negatives and would make
the annotation more effective.

### Interpretation
An interpretation for why the results of the validation set perform much worse then the training
set is that there is a bias in the training set. The prompt training set was annotated by searching
for polarizing terms and labelling these search examples. The training set then contains a larger
amount of examples that have words in that are more clearly polarizing. It is then easier for GPT
models to predict correctly examples that have these polarizing terms.

Furthermore, after doing manual research I would like to suggest that often it is hard to annotate
for a binary (accept, reject) definition of polarization. There is often a level of ambiguity in
polarization that might be better incorporated in the classification. As such it might be more
effective and give the user of the predictions more transparency if the classification is a rating
of polarization (IE. 0-10). Predictions might then be able to allow for a margin of error that the
user of the classifications would still find useful. For instance if predictions have an error of
1.5 in a scale of 0-10 a user could still filter for different levels of polarization in a corpus
and it could still be useful when searching for a subset of a large corpus that an end user wants
to focus in on.

The model gpt-4-1106-preview has a much lower cost and a similar performance to gpt-4 and would be
recommended as the best performing model.

### Limitations
- The current outputs of the model are defined as a function. It might also give different results
  if the output of the model is changed. For example different function definitions or text as an
  output.
- Due to there being one annotator for an classification that is complex it is hard to gauge the
  validity of the validation set.

### Further research
Further research needs to be done it to:
- Have a single or number of other experts annotate the validation set. An optimisation of this is
  to annotator only false positives and false negatives for the current validation results.
- Look in to if classifying on how polarizing, a scales of polarization, rather then if an example
  is polarizing, binary classification, might make for better and more useful results.
