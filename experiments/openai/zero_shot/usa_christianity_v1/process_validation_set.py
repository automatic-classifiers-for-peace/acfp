"""Process the sample set to create classification_labels."""
import pandas as pd

import logging

from acfpopenai import form

SHARED_DATA_PATH = "../../../../shared_data/buildup/usa_christianity/"


def main():
    """Process the sample set to create classification_labels."""
    training_labels = pd.read_csv("prompt_training_classification_labels.csv")
    training_labels = training_labels.set_index("text_snippet_id")
    training_labels["class_name"] = training_labels["class_name"].astype("category")
    training_labels["labeled_at"] = training_labels["labeled_at"].astype(
        "datetime64[ns]"
    )
    all_labels = pd.read_csv(SHARED_DATA_PATH + "classification_labels.csv")
    all_labels = all_labels.set_index("text_snippet_id")
    all_labels["class_name"] = all_labels["class_name"].astype("category")
    all_labels["labeled_at"] = all_labels["labeled_at"].astype("datetime64[ns]")
    logging.info(
        "Classes in usa_christianity annotations %s",
        all_labels["class_name"].value_counts(),
    )
    classes_to_keep = [
        "is_attitude_polarization_accept",
        "is_attitude_polarization_reject",
        "is_attitude_polarization_ignore",
    ]
    all_text_snippets = pd.read_csv(SHARED_DATA_PATH + "text_snippets.csv")
    all_text_snippets = all_text_snippets.set_index("id")

    validation_labels, validation_text_snippets = form.form_validation_set(
        training_labels, all_labels, all_text_snippets, classes_to_keep
    )
    logging.info(
        "Size prompt_training_classification_labels %s", training_labels.shape[0]
    )
    logging.info("All text count %s", all_text_snippets.shape[0])
    logging.info("Size validation_text_snippets %s", validation_text_snippets.shape[0])
    validation_text_snippets.to_csv("validation_text_snippets.csv")
    validation_labels.to_csv("validation_classification_labels.csv")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
