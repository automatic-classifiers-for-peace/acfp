# Open AI

Experiments using [Open AI](https://openai.com/).

## OpenAI library and running experiment scripts
Currently there is a simple ACfP OpenAI (`acfpopenai`) lib. You will need to install this to use do the experiments:
- Set up the development environment as in [`/development.md`](/development.md)
- `cd experiments/openai/`
- `pip install -r requirements.txt`
- `pip install -e .`

There are a number of scripts that can be run. To see help for the script run the commands:
- `python acfpopenai/classification.py`
- `python acfpopenai/parsing.py`
