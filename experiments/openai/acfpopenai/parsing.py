"""Script for parsing the raw openai classification in to valid classification_labels."""

import argparse
import json

import pandas as pd

import logging

from acfpopenai import chains

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def run(
    filepath: str,
    class_name_for_positive: str,
    class_name_for_negative: str,
    output_file_path: str,
    chain_id: str,
):
    """Run the parsing."""
    if chain_id != chains.DEFAULT_CHAIN:
        raise ValueError(
            "This script is only for the default chain. "
            "Please use the correct script for the chain you want to use."
        )
    logger.info("Reading file %s", filepath)
    input_df = pd.read_csv(filepath)
    logger.info("Total size of input: %s", input_df.shape)
    logger.info("Parsing the raw classification")
    output_df = input_df.copy()
    response_df = output_df["gpt_response"].apply(lambda x: json.loads(x))
    response_df = pd.json_normalize(response_df)
    logging.info(response_df.head())
    response_add_df = response_df["additional_kwargs.function_call.arguments"].apply(
        lambda x: json.loads(x)
    )
    response_add_df = pd.json_normalize(response_add_df)
    output_df["is_class"] = response_add_df["is_class"]
    output_df["certainty"] = response_add_df["certainty"]
    output_df["class_name"] = class_name_for_negative
    output_df["class_name"] = output_df["class_name"].mask(
        output_df["is_class"].isin([True]), class_name_for_positive
    )
    classification_predictions = output_df[["id", "class_name", "gpt_responsed_at"]]
    classification_predictions = classification_predictions.rename(
        columns={
            "id": "text_snippet_id",
            "gpt_responsed_at": "predicted_at",
        }
    )
    classification_predictions["model_id"] = (
        output_df["model_id"] + "-" + output_df["prompt_file_path"]
    )
    classification_predictions = classification_predictions.set_index("text_snippet_id")
    classification_predictions.to_csv(output_file_path)
    logging.info(classification_predictions.head())


default_parser = argparse.ArgumentParser(
    description="Script for parsing the raw openai classification in to valid classification_labels."
)

default_parser.add_argument("filepath", type=str, help="Path to the input CSV file.")
default_parser.add_argument(
    "class_name_for_positive",
    type=str,
    help="The class_name that should be added positive predictions",
)
default_parser.add_argument(
    "class_name_for_negative",
    type=str,
    help="The class_name that should be added negative predictions",
)
default_parser.add_argument(
    "output_file_path", type=str, help="Path to the output file."
)
default_parser.add_argument(
    "--chain_id",
    type=str,
    help=f"Chain id. See src/chains.py. Default is {chains.DEFAULT_CHAIN}",
    default=chains.DEFAULT_CHAIN,
)

if __name__ == "__main__":
    args = default_parser.parse_args()
    run(
        args.filepath,
        args.class_name_for_positive,
        args.class_name_for_negative,
        args.output_file_path,
        args.chain_id,
    )
