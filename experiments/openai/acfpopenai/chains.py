"""Registry of chains."""
from typing import Callable

from langchain import chat_models
from langchain.prompts import chat
from langchain.schema.runnable import base

DEFAULT_CHAIN = "general_classify_function"


general_classify_function = {
    "name": "classify",
    "description": "Classify a text",
    "parameters": {
        "type": "object",
        "properties": {
            "is_class": {
                "type": "boolean",
                "description": "Whether the text is in the classification",
            },
            "certainty": {
                "type": "number",
                "description": "How certain the model that the text is in that classification, 0.0-1.0",
            },
        },
        "required": ["is_class", "certainty"],
    },
}


def general_classify_function_chain(model_id: str, prompt: str) -> base.Runnable:
    """Create a chain."""
    chat_model = chat_models.ChatOpenAI(temperature=0, model=model_id).bind(
        function_call={"name": "classify"}, functions=[general_classify_function]
    )
    system_message_prompt = chat.SystemMessagePromptTemplate.from_template(
        "You are a helpful assistant."
    )
    human_template = prompt + " {text}"
    human_message_prompt = chat.HumanMessagePromptTemplate.from_template(human_template)
    chat_prompt = chat.ChatPromptTemplate.from_messages(
        [system_message_prompt, human_message_prompt]
    )

    chain = chat_prompt | chat_model
    return chain


def simple_text_chain(model_id: str, prompt: str) -> base.Runnable:
    """Create a chain."""
    chat_model = chat_models.ChatOpenAI(temperature=0, model=model_id)
    system_message_prompt = chat.SystemMessagePromptTemplate.from_template(
        "You are a helpful assistant."
    )
    human_template = prompt + " {text}"
    human_message_prompt = chat.HumanMessagePromptTemplate.from_template(human_template)
    chat_prompt = chat.ChatPromptTemplate.from_messages(
        [system_message_prompt, human_message_prompt]
    )
    chain = chat_prompt | chat_model
    return chain


CHAIN_REGISTRY: dict[str, Callable[[str, str], base.Runnable]] = {
    "general_classify_function": general_classify_function_chain,
    "simple_text": simple_text_chain,
}


def get_chain(chain_id: str, model_id: str, prompt: str) -> base.Runnable:
    """Get a chain."""
    if chain_id not in CHAIN_REGISTRY:
        raise ValueError(f"Chain {chain_id} not found")
    return CHAIN_REGISTRY[chain_id](model_id, prompt)
