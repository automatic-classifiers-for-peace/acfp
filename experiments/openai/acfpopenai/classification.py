"""Classification functionality."""
import argparse
import datetime
import os
from functools import partial

import pandas as pd
import logging

from acfpopenai import chains

import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


DEFAULT_MODEL = "gpt-3.5-turbo"

# The number of records to process before a persist is done
# This means that if you stop the process half way through
# All the previous batches will be persisted
BATCH_PERSIST_SIZE = 1


def form_text_data_to_process(
    input_csv_path: str, start_offset=0, number_of_records_to_process=None
):
    """Get the text data to process."""
    logger.info("Reading file %s", input_csv_path)
    input_df = pd.read_csv(input_csv_path)
    logger.info("Total size of input: %s", input_df.shape)
    if number_of_records_to_process is None:
        input_df = input_df[start_offset:]
    if number_of_records_to_process is not None:
        input_df = input_df[start_offset : start_offset + number_of_records_to_process]
    logger.info("Size of input after slicing: %s", input_df.shape)
    return input_df


def compute(chain, chunked_df_to_process):
    output_df = chunked_df_to_process.copy()
    output_df["gpt_response"] = output_df["gpt_text_to_process"].apply(
        lambda x: chain.invoke({"text": x}).json()
    )
    output_df["gpt_responsed_at"] = datetime.datetime.now(datetime.timezone.utc)
    return output_df


def run(
    input_csv_path,
    prompt_file_path,
    output_file_path,
    chain_id: chains.DEFAULT_CHAIN,
    model=DEFAULT_MODEL,
    start_offset=0,
    number_of_records_to_process=None,
    sleep_between_batches=None,
):
    """Run the main script."""
    logger.info("Running for model: %s", model)
    text_df_to_process = form_text_data_to_process(
        input_csv_path, start_offset, number_of_records_to_process
    )
    text_df_to_process["gpt_text_to_process"] = text_df_to_process["text"]
    text_df_to_process["model_id"] = model
    text_df_to_process["prompt_file_path"] = prompt_file_path
    logger.info("Sample of input data: %s", text_df_to_process.head())
    with open(prompt_file_path) as f:
        prompt = f.read()
    logger.info("Prompt to use %s", prompt)

    chain = chains.get_chain(chain_id, model, prompt)
    logger.info("Chian %s", chain)
    first_time_with_header = False
    if not os.path.exists(output_file_path):
        first_time_with_header = True

    dfs = []
    compute_partial = partial(compute, chain)
    chunks = [
        text_df_to_process[i : i + BATCH_PERSIST_SIZE]
        for i in range(0, text_df_to_process.shape[0], BATCH_PERSIST_SIZE)
    ]
    for chunk in chunks:
        result_df = compute_partial(chunk)
        if first_time_with_header:
            result_df.to_csv(output_file_path, mode="a", header=True)
            first_time_with_header = False
        else:
            result_df.to_csv(output_file_path, mode="a", header=False)
        logger.info("Classified range %s", result_df.index)
        dfs.append(result_df)
        if sleep_between_batches is not None:
            time.sleep(sleep_between_batches)

    total_processed = pd.concat(dfs)
    logger.info("Total processed %s", total_processed.shape[0])
    total_processed.head()


default_parser = argparse.ArgumentParser(description="Run the Open AI experiments.")
default_parser.add_argument("filepath", type=str, help="Path to the input CSV file.")
default_parser.add_argument(
    "prompt_file_path", type=str, help="Path to the prompt file."
)
default_parser.add_argument(
    "output_file_path", type=str, help="Path to the output file."
)
default_parser.add_argument(
    "--chain_id",
    type=str,
    help=f"Chain id. See src/chains.py. Default is {chains.DEFAULT_CHAIN}",
    default=chains.DEFAULT_CHAIN,
)
default_parser.add_argument(
    "--model_id",
    type=str,
    help=f"Model ID to use. Default is {DEFAULT_MODEL}",
    default=DEFAULT_MODEL,
)
default_parser.add_argument(
    "--start_offset", type=int, help="Start offset for the input file.", default=0
)
default_parser.add_argument(
    "--number_of_records_to_process",
    type=int,
    help="Number of records to process from the input file.",
)
default_parser.add_argument(
    "--sleep_between_batches",
    type=int,
    help="Sleep between batches.",
)

if __name__ == "__main__":
    args = default_parser.parse_args()
    run(
        args.filepath,
        args.prompt_file_path,
        args.output_file_path,
        args.chain_id,
        args.model_id,
        args.start_offset,
        args.number_of_records_to_process,
        args.sleep_between_batches,
    )
