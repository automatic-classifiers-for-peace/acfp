"""Form functionality."""
import pandas as pd

from acfp import schemas


def form_validation_set(
    training_labels: pd.DataFrame,
    all_labels: pd.DataFrame,
    all_text_snippets: pd.DataFrame,
    classes_to_keep: list[str],
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Create a validation set from all the annotations excluding the training set.

    Args:
        training_labels: The training labels.
        all_labels: All labels.
        all_text_snippets: All text snippets.
        classes_to_keep: The classes to keep.

    Returns:
       A tuple of the validation labels and the validation text snippets.
    """
    schemas.classification_labels.validate(training_labels)
    schemas.classification_labels.validate(all_labels)
    schemas.text_snippets.validate(all_text_snippets)
    validation_labels = all_labels[all_labels["class_name"].isin(classes_to_keep)]
    validation_labels = validation_labels[
        ~validation_labels.index.isin(training_labels.index)
    ]
    validation_text_snippets = all_text_snippets[
        all_text_snippets.index.isin(validation_labels.index)
    ]
    return validation_labels, validation_text_snippets
