"""Metrics for the acfpopenai."""
from typing import Optional

import pandas as pd
import numpy as np
from sklearn import metrics


def binary_f1_score(
    pos_class_name: str,
    labeled_by: str,
    classification_labels: pd.DataFrame,
    classification_predictions: pd.DataFrame,
    classes: Optional[list[str]] = None,
) -> pd.DataFrame:
    """Compute the accuracy for each class name."""
    labels = classification_labels[["labeled_by", "class_name"]]
    if classes is not None:
        labels = labels[labels["class_name"].isin(classes)]
    labels = labels[labels["labeled_by"].isin([labeled_by])]
    labels["label"] = labels["class_name"].isin([pos_class_name])

    predictions = classification_predictions[["model_id", "class_name"]]
    if classes is not None:
        predictions = predictions[predictions["class_name"].isin(classes)]
    predictions["prediction"] = predictions["class_name"].isin([pos_class_name])
    models = predictions["model_id"].unique()

    result = []

    for model_id in models:
        model_predictions = predictions[predictions["model_id"] == model_id]
        predictions_labels_df = model_predictions.merge(
            labels, left_index=True, right_index=True
        )
        f1_score = metrics.f1_score(
            predictions_labels_df["label"],
            predictions_labels_df["prediction"],
        )

        predicted = labels.index.isin(predictions_labels_df.index)
        accuracy = {
            "model_id": model_id,
            "f1_score": f1_score,
            "coverage": np.sum(predicted) / len(predicted) * 100,
        }
        result.append(accuracy)

    return pd.DataFrame(result)
