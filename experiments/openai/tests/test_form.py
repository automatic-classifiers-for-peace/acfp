"""Test form functionality."""
import datetime
import pandas as pd

from acfpopenai import form


def test_form_validation_set():
    """Test form_validation_set."""
    training_labels = pd.DataFrame(
        {
            "labeled_at": [datetime.datetime.now()] * 5,
            "labeled_by": ["labeled_by"] * 5,
            "class_name": ["a", "b", "c", "d", "e"],
            "text_snippet_id": [1, 2, 3, 4, 5],
        }
    )
    training_labels = training_labels.set_index("text_snippet_id")
    training_labels["class_name"] = training_labels["class_name"].astype("category")
    all_labels = pd.DataFrame(
        {
            "labeled_at": [datetime.datetime.now()] * 10,
            "labeled_by": ["labeled_by"] * 10,
            "class_name": ["a", "b", "c", "a", "b", "c", "a", "b", "c", "d"],
            "text_snippet_id": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        }
    )
    all_labels = all_labels.set_index("text_snippet_id")
    all_labels["class_name"] = all_labels["class_name"].astype("category")
    all_text_snippets = pd.DataFrame(
        {
            "text": [
                "a",
                "b",
                "c",
                "d",
                "e",
                "f",
                "g",
                "h",
                "i",
                "j",
            ],
            "id": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        }
    )
    all_text_snippets = all_text_snippets.set_index("id")
    classes_to_keep = ["a", "b", "c"]
    validation_labels, validation_text_snippets = form.form_validation_set(
        training_labels=training_labels,
        all_labels=all_labels,
        all_text_snippets=all_text_snippets,
        classes_to_keep=classes_to_keep,
    )
    assert validation_labels.shape[0] == 4
    assert validation_text_snippets.shape[0] == 4
    assert validation_labels.index.equals(validation_text_snippets.index)
    assert validation_labels["class_name"].isin(classes_to_keep).all()
    assert (~validation_labels.index.isin(training_labels.index)).all()
    assert (~validation_text_snippets.index.isin(training_labels.index)).all()
