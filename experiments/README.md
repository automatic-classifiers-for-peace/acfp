# Experiments

This folder contains experiments that have been done by the ACfP.

Experiments are easily replicable experimentation with classifications or methodologies for
creating classifications.

Currently we are working on these experiments: https://docs.google.com/document/d/1SYJy5jw9QvpawxpDCODuq7odsHq9zYxTOU_KpCZQg-g/edit

In general an experiment should:
- follow the principles of the ACfP
- have a unique folder name/id
- be as easy as possible to replicate
- have all the code and dependencies included in the experiment folder
- have any datasets used accessible, even if only on request, but publicly/self-serve is better
- have results and conclusions of the experiment written up

See [./template/](./template/) for a starting point when creating new experiments.

## Completion and depreciation
Once an experiment has been completed a git tag will be added to the repo. This git tag should be
indicated in an experiment's README.md. When someone is replicating or re-running an experiment
they should checkout this version of the code.

Convention for the tag of experiment:
`<id>/<version>`, where:
* `id` is the same as the folder. eg. `llms/openai`
* `version` is a simple version starting at 1. +1 for every new version.

Using this simple system all experiments can be depreciated and re-run without problems while being
able to hold the history of experimentation in the repo.
