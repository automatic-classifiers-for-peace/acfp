"""Testing configuration.

The configuration in this file adds a series of 'guards' on to all tests.
They are:
    - No network calls: Unit tests should all pass offline.
      Monkypatches `socket` module to attempt to guard against network
      calls in test.
"""
import socket

import pytest


def network_call_guard(*args, **kwards):
    """Raise an exception if a network call is attempted."""
    raise Exception("Network call attempted, disallowed in tests.")


@pytest.fixture(autouse=True)
def enforce_network_call_guard(monkeypatch):
    """Autouse fixture to enforce guarding against network calls in test."""
    monkeypatch.setattr(socket.socket, "connect", network_call_guard)
