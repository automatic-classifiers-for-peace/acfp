"""DataFrame Schemas."""
import pandera as pa

text_snippets = pa.DataFrameSchema(
    index=pa.Index(pa.Int, name="id"),
    columns={
        "text": pa.Column(pa.String),
        # These columns are not required because some times they are not in the raw annotation
        # data.
        "source": pa.Column(pa.String, required=False),
        "source_id": pa.Column(pa.Int, required=False),
    },
)

classification_labels = pa.DataFrameSchema(
    index=pa.Index(pa.Int, name="text_snippet_id"),
    columns={
        "class_name": pa.Column(pa.Category),
        # Labeled by should be in the format:
        # "<ORG_ID>_<INITIALS_OF_ANNOTATOR>"
        "labeled_by": pa.Column(pa.String),
        "labeled_at": pa.Column(pa.Timestamp),
    },
)

classification_predictions = pa.DataFrameSchema(
    index=pa.Index(pa.Int, name="text_snippet_id"),
    columns={
        "class_name": pa.Column(pa.Category),
        "predicted_at": pa.Column(pa.Timestamp),
        "model_id": pa.Column(pa.String),
    },
)

classes = pa.DataFrameSchema(
    index=pa.Index(pa.Category, name="class_name"),
    columns={
        "description": pa.Column(pa.String),
    },
)
