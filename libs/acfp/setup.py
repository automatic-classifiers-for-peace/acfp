"""Setup file for the acfp library.

Needed for compatibility with legacy builds as stated in:
https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html
"""
import setuptools

setuptools.setup()
