# Automatic Classifiers for Peace (ACfP) library

The intention is that this library is installed into each experiment, to give shared functionality across experiments. Like common data processing, interfaces, and tasks.

It would then contain:
- definitions of common data structures
- functionality to process annotated data into storage
- functionality to get the data sets from storage

## Usage
To install this library in an experiment use:
`pip install -e .`
or if you are in a different sub folder:
`pip install -e <path to this folder>`

## Development
See `/development.md` (in root of this repo) for the generalised development instructions.
