# Libraries of Automatic Classifiers for Peace (ACfP)

These libraries make up the shared body of code that can be used across experiments and in the
future productionised classifiers.

## Adding a new library
To do this:
- make a new folder `libs/my_new_lib`
- Copy the below stated files from `libs/acfp/`
- Edit the copied files to have the new name and correct information.
- Add the `pre-commit` hooks for the new library to `.pre-commit-config.yaml` (in root directory).
  See `.pre-commit-config.yaml` for more information.
- Add a `test_my_new_lib` to `.gitlab-ci.yml` (in root directory). Use `test_acfp` as example.

### To copy from `libs/acfp`:
- `pyproject.toml`
- `setup.py`
- `tests/__init__.py`
- `tests/test_dummy.py`
- `README.md`
