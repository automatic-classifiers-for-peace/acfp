# A note about the linting and formatting
# There a number of rules that are in preview in ruff and not in the
# default ruff rules.
# Lots of these are pycodestyle:
# https://docs.astral.sh/ruff/rules/#error-e (the test tube sign indicates
# preview)
# Further issue about this in ruff
# https://github.com/astral-sh/ruff/issues/5873
#
# It would be could be possible to add `--preview` to ruff but there are still a
# number of rules that not are not fixed but the formatting of ruff (E252).
#
# These rules seem to be basic formatting as such black is used in conjunction with ruff.
.PHONY: all lint typecheck test

# Use `path` argument to specify the library to run the command on.
# make <command> path=libs/acfp/
path = libs/acfp/

all: lint typecheck test

lint:
	@echo "Running linter for path $(path)..."
	cd $(path) && ruff check . && black --check .

typecheck:
	@echo "Running typecheck for path $(path)..."
	cd $(path) && mypy .

test:
	@echo "Running tests for path $(path)..."
	cd $(path) && pytest tests

format:
	@echo "Running format for path $(path)..."
	cd $(path) && black . && ruff check --fix .

dev_install:
	@echo "Running dev install for path $(path)..."
	pip install -r requirements.dev.txt
	pip install -r $(path)requirements.txt
	pip install -e $(path)

dev_compile:
	@echo "Running dev compile..."
	pip-compile requirements.dev.in

dev_upgrade:
	@echo "Running dev upgrade..."
	pip-compile --upgrade requirements.dev.in

compile: dev_compile
	@echo "Running compile for path $(path)..."
	pip-compile $(path)pyproject.toml

upgrade: dev_upgrade
	@echo "Running compile for path $(path)..."
	pip-compile --upgrade $(path)pyproject.toml
