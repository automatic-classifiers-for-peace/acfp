# Principles of the Automatic Classifiers for Peace group

These principles give guidelines and direction for collaborating and developing the repository.

## General Principles
* We prioritise the understandability and usability of everything do
* We support and respect one another: we can be tough on ideas but are always gentle on people
* We are not violent or derogatory
* We resolve conflict through peaceful and pragmatic ways
* We tend towards combining already existing tools and ideas rather than coming up with our own
* We work asynchronous and are aware of the requirements for this, [more info in a blog from gitlab](https://about.gitlab.com/company/culture/all-remote/asynchronous/#how-does-asynchronous-communication-work)
* We aim to be concise and considerate in our communication
* There is no right and wrong, there are advantages and disadvantages

## Coding Principles
* We prioritise the understandability and usability of everything do
* We aim to write documentation first, have that reviewed and then implement the feature
* We try to make small understandable changes: with commits that are atomic and MRs (PRs) that are like short stories for the reviewer, see below for resources
* We attempt to make software tools that follow the Unix philosophy of “Doing one thing well”, [more info here](http://www.catb.org/~esr/writings/taoup/html/ch01s06.htm)

### Further reading and resources:
* On MRs (PRs): https://wiki.crdb.io/wiki/spaces/CRDB/pages/1411744698/Organizing+PRs+and+Commits
