# Contributing

Hi there! We're thrilled that you'd like to contribute to this group. Your help is essential for keeping it great.
Please note that all contributions fall under the [license of the project](./LICENSE).

Contributions can come in different forms:
* Helping with documentation and understandability of how to use automatic classifiers for peacebuilders
* Helping with code and the building of tools
* Helping maintain the community

## How to get started:
* Read our principles of ACfP document: [./principles.md](./principles.md)
* Read the ["Contributions to the repo"](./docs/contributing_to_the_repo.md)
* Join the conversation through our [Discord server](https://discord.gg/vhgtS73)

# Contributing to the repo

There are two ways to make contributions to this codebase:
* Issues
* MRs (PRs)

## Issues:
Issues can be made through the GitLab interface, see [GitLab documentation](https://docs.gitlab.com/ee/user/project/issues/)

## MRs:
First have a look at the "Coding Principles" for our [principles](../principles.md).

Follow the create an [MR documentation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html). You can also use the "Fork workflow" if you are not a "Developer" or "Maintainer" of the repo.

## Developing
See [development.md](development.md) for more information on the development environment.
