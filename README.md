# Automatic Classifiers for Peace

The automatic classifiers for peace group is an open-source community of researchers, organisations and individuals that are interested in aiding social media analysis through the automatic classification of social media discourse.

This repository contains the experiments, documentation, tutorials and source code that the group has developed.

## Usage
The repository should be explored as one would explore a folder. A good place to start in a
subfolder is that folder's `README.md`.

## Goals of the repository
The repository is aiming to be a space/tools/platform that can allow for the sharing of work
between different researchers and organisations that can collaborate in a way that is:
- publicly visible
- other people/organisations can easily join in
- allows for the sharing of data in a way that fits the sensitivity of the data
- allows for the formalisation of shared code that can lead to high-quality libraries
- work towards products/classification models could be publicly used

## Parts of the repository
### Shared data (./shared_data/)
A space where shared annotations and other data that data owners can say "They have this shared
data that others can use" but that they can control the access to who can use the data and see the
actual rows of the data.

### Experiments (./experiments/)
A space where people can propose and show experiments for automatic classifier for peace. This is a
place with little formalised structure on how code should be written. A place where anyone, no
matter the technical level, can add research they have been doing so that others can help out.

### Libraries (./libraries/)
A space where formalised quality-driven code can be shared across experiments (and hopefully
beyond). That it could be natural and easy to move code from an experiment into a library. To start
to build shared code and knowledge to formalise the ways of working, making it easier and more
likely for success.

### Knowledge base ([wiki](https://gitlab.com/automatic-classifiers-for-peace/acfp/-/wikis/home))
A place where people can understand the ideas and theory about building automatic classifiers for
peace. With the idea that it can be a starting point for people that want to get involved but don't
yet have the knowledge to do so.

### Products (still to do)
At some point in the future, we hope to release open-source free products that can be used by
peacebuilders and people that analyse conflicts.
