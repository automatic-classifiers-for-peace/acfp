# Development
This document outlines the generalised development flow and environment for the repo. This
development flow must be used for all libraries in `libs/`.

The development tools could also be used for an experiment however this is up to the developers
discretion. Experiments are free spaces for developers to work as they see best.

## Environment
Library developers need to setup a local development environment. It is recommended to use `pyenv`
to manage local python versions - the `.python-version` in the root folder (`.python-version`)
automatically ensure the correct python version is used.

Developers are free to use their python virtual environment manager of choice. Python's standard
library `venv` is recommended. (a venv with the name `.venv` can be created with command `python -m
venv .venv`, and then activated with `source .venv/bin/activate` or `.venv\Scripts\activate` if using Windows).

To complete the environment install the development dependency by running: `make dev_install`

## `make`
There is a `make` interface that is configured in `Makefile`. Use `make <command> path=<path to
code>` to run the make on different libraries. The `path` option will default to `libs/acfp/` and
can omitted from `make` commands when working on `libs/acfp/`.

### `make` on Windows
Disclaimer: These instructions for make on windows have not been tested, so if you get this working
please amend them.

To be able to use `make` on Windows, you need to install Chocolatey, a package manager for Windows.
Follow the instructions [here](https://chocolatey.org/install).

To install `make` for Windows, run the following command from the command line or from PowerShell:
`choco install make`.

## Package management
Package management (version pinning, version updating) is handled using the utility commands `make
compile` and `make upgrade`, which uses `pip-tools` - install with `make dev_install`.

To add a new requirement to the package add the package to the dependency to the `pyproject.toml`
in the specific lib you want to add it to - then run `make compile`, and `make dev_install`. To add
a development only package, add the package to the requirements.dev.in` file, then run
`make compile`, and `make dev_install`.

To update the requirements to the latest version run `make upgrade` and `make dev_install`.

## Workflow

When working on a library make changes to the library and then run `make` (`make path=<path>`).
This will then run the checks to see if the changes you made adhere to the configured code quality
tools. `make format` can also be used to have the formatter fix linting issues.

## Pre-commit
[`pre-commit`](https://pre-commit.com/) is configured in the project and is recommended to be used
for you work flow. To set this up:
- set up the development environment as above
- `pre-commit install`
- `git commit` will then run main linting before a commit

Be aware that you will need to run `git commit` in the virtual environment, if you have edited
files in a lib, otherwise pre-commit typecheck will fail.

## CI pipeline
A CI pipeline has been set up so that code quality checks will automatically be run when changes to
libs has been made. See `.gitlab-ci.yml` for more information.
