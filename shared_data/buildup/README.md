# Build Up shared data

[Build Up](https://howtobuildup.org/) is a digital peacebuilding NGO that, together with
[datavaluepeople](https://datavaluepeople.com/), helps to facilitate the Automatic Classifiers for
peace group.

The data here can be used by anyone that is building automatic classifiers for peace.

## Access
Before you can pull the data you need to have access to the remote storage. To do this:
- Read and understanding the Build Up data policy,
  [here](https://howtobuildup.org/wp-content/uploads/2022/09/Build-Up-Data-Protection-Policy-Final-220831-signed.pdf)
- Download and sign the data processor contract, TODO
- Email `buildup@datavaluepeople.com` with the signed data processor contract and a motivation
  letter for why you want to be able to use the data
- Once your request has been approved you will then receive an email asking to signup to AWS with
  the subject "Invitation to join AWS IAM Identity Center"
- Follow the instructions in the email and sign up

### `aws`configuration
Once you have a user to the build up AWS account you need to configure `aws` and `dvc`
- Install the [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) if you don't have it.
- Configure your sso for aws:
  - `aws configure sso`
  - Answering the questions:
  - `SSO session name (Recommended):` as `acfp-ss`
  - `SSO start URL [None]:` as `https://buildup.awsapps.com/start`
  - `SSO region [None]:` as `us-east-1`
  - `SSO registration scopes [sso:account:access]:` as default so press "Enter"
- You will be asked to login on the browser. 
- Return to your terminal and continue answering the questions:
  - `CLI default client Region [us-east-1]:` as default so press "Enter"
  - `CLI default output format [json]:` as default so press "Enter"
  - `CLI profile name ...` as `acfp`
- Check you can read the bucket: `aws s3 ls --profile acfp s3://acfp-buildup-shared-data-v1`
  - You should see a list of files like `shared_data`  

### `dvc` configuration
In order to use `dvc` you'll need to create a virtual environment. Here you have two options:
- If you are planning to participate in development or run experiments, you need to set up your dev environment according to [these instructions](../../development.md#Environment).
- If you are only interested in downloading datasets then you can do the following:
  - Create a new virtual environment by running `python -m venv .venv`, then activating it with `source .venv/bin/activate` (or if you are using Windows, activate it with `.venv\Scripts\activate`).
  - Install `dvc` by running `pip install dvc` and `pip install dvc-s3`.   
- Configure the profile for `dvc`:
  - Change the current directory of the terminal to this folder: `cd <path to acfp repo>/shared_data/buildup`
  - Run `dvc remote modify --local acfp-buildup-shared-data profile acfp`
  - `dvc list .` should list the data files.


## Usage
Once you have access you can pull all the data locally with: `dvc pull .`.

IMPORTANT: always follow the ["Important data protection measure"](../README.md#important-data-protection-measure) section in the general README.md
