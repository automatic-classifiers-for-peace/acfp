# USA Christianity polarisation

This dataset supports the peacebuilding objective: Understand to what extent is Christian identity
as expressed on social media furthering polarization of attitudes towards others and mistrust of
others in the USA.

Based on this, looking at:
- 20 twitter influencers who regularly express Christian identity, across two spectrums –
  polarizing v. pluralizing, red v. blue
- 10 phrases that people on Facebook use to express Christian identity and based on this search for
  top performing pages that use those phrases whose admin is in the US

And classifying for:
- Relevance: references Christianity
- Attitude polarization: perceptual shifts towards stereotype, vilification, dehumanization, or
  deindividuation of others

## Raw Annotations
The data in ./raw_annotations/ is data produced through prodigy that was annotated by Build Up
around November 2023.

## Validated Dataset
The `text_snippets.csv`, `classification_labels.csv` and `classes.csv` are validated dataframes using the acfp schemas. They have all been produced using
the script `process_raw_data.py`. Any changes to the datasets should be done using the script and not by hand.

### text_snippets
The ids of the text_snippets are their alphabetical ordering as there is no source id in the raw
data.

## Process raw data
The raw data has been processed using the `process_raw_data.py` script. To run this script:
- Set up the development environment as in [`/development.md`](/development.md)
- `pip install pyarrow`
- run `python shared_data/buildup/usa_christianity/process_raw_data.py`
