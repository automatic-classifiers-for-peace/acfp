"""Process the raw annotations into shared schemas."""
import pandas as pd
from pathlib import Path

from acfp import schemas

ANNOTATOR_ID = "BU_JH"


DESCRIPTION_DF = pd.DataFrame(
    [
        [
            "is_christianity_accept",
            "The text is about Christianity."
        ],
        [
            "is_christianity_reject",
            "The text is not about Christianity."
        ],
        [
            "is_christianity_ignore",
            "The labeller ignored this text snippet when classifying for Christianity."
        ],
        [
            "is_attitude_polarization_accept",
            "The text has attitude polarization."
        ],
        [
            "is_attitude_polarization_reject",
            "The text does not contain attitude polarization."
        ],
        [
            "is_attitude_polarization_ignore",
            "The labeller ignored this text snippet when classifying for attitude polarization."
        ],
    ],
    columns=["class_name", "description"]
)


def format_classification_labels(df):
    """Format the classification labels."""
    df["class_name"] = (
        df["label"] + "_" + df["answer"]
    ).astype("category")
    df["labeled_by"] = ANNOTATOR_ID
    df["labeled_at"] = pd.to_datetime(df["_timestamp"], unit="s")
    df = df.set_index("text_snippet_id")
    df = df[
        ["class_name", "labeled_by", "labeled_at"]
    ]
    return df


def _process_raw_df(df):
    df = df.sort_values("text")
    df = df.reset_index(drop=True)
    return df


def form(raw_facebook_df, raw_twitter_df):
    """Process the raw data."""
    # Format the raw_facebook_df
    raw_facebook_df["platform"] = "facebook"
    # The prodigy annotations were configured to have the `User <user_id>: \n\t` As such we need to
    # remove this to get the text snippet.
    df_temp = raw_facebook_df["text"].str.split(": \n\t", n=1, expand=True)
    raw_facebook_df["text"] = df_temp[1]
    raw_twitter_df["platform"] = "twitter"
    # Index is dependent on the order of the dataframes so much remain the same.
    df = pd.concat([_process_raw_df(raw_facebook_df), _process_raw_df(raw_twitter_df)])

    # Produces the text_snippets
    text_snippets = df[["text", "platform"]]
    # Since there are two classification here: is_christianity and is_attitude_polarization
    # we need to de-duplicate the text snippets.
    text_snippets = text_snippets.drop_duplicates()
    text_snippets = text_snippets.reset_index(drop=True)
    text_snippets.index.set_names("id", inplace=True)
    text_snippets_join = text_snippets.reset_index()

    classification_labels = df[["text", "label", "answer", "_timestamp", "platform"]]
    # Incase there are two texts the same we have to join on platform
    classification_labels = classification_labels.merge(
        text_snippets_join, how="left", on=["text", "platform"]
    )
    classification_labels = classification_labels.rename(columns={"id": "text_snippet_id"})

    classes = classification_labels[["label", "answer"]].drop_duplicates()
    classes = classes.reset_index(drop=True)
    classes["model_id"] = "buildup_usa_christianity_polarization"
    classes["class_name"] = classes["label"] + "_" + classes["answer"]
    classes = classes.join(DESCRIPTION_DF.set_index("class_name"), on="class_name")
    classes = classes[["model_id", "class_name", "description"]]
    classes = classes.sort_values("class_name")
    classes["class_name"] = classes["class_name"].astype("category")
    classes = classes.set_index("class_name")

    classification_labels = format_classification_labels(classification_labels)
    # Validation
    schemas.text_snippets.validate(text_snippets)
    schemas.classes.validate(classes)
    schemas.classification_labels.validate(classification_labels)
    return text_snippets, classes, classification_labels


def main():
    """Process the raw data.

    This will process the raw data files:
    - ./raw_annotations/bu_christianity_facebook.jsonl
    - ./raw_annotations/bu_christianity_twitter.jsonl

    This will produce the following files:
    - text_snippets.csv
    - classes.csv
    - classification_labels.csv

    Which are validated dataframes of the types with the same name as acfp/schemas.py.
    """
    this_dir = Path(globals().get("__file__", "./_")).absolute().parent
    raw_facebook_df = pd.read_json(
        this_dir / "raw_annotations" / "bu_christianity_facebook.jsonl", lines=True
    )
    raw_twitter_df = pd.read_json(
        this_dir / "raw_annotations" / "bu_christianity_twitter.jsonl", lines=True
    )

    text_snippets, classes, classification_labels = form(
        raw_facebook_df, raw_twitter_df
    )

    print("text_snippets")
    print(text_snippets.head())
    print("classes")
    print(classes.head())
    print("classification_labels")
    print(classification_labels.head())
    text_snippets.to_csv(this_dir / "text_snippets.csv")
    classes.to_csv(this_dir / "classes.csv")
    classification_labels.to_csv(this_dir / "classification_labels.csv")


def test_form():
    """Test the form function."""
    this_dir = Path(globals().get("__file__", "./_")).absolute().parent
    raw_facebook_df = pd.read_json(
        this_dir / "raw_annotations" / "test_facebook.jsonl", lines=True
    )
    raw_twitter_df = pd.read_json(
        this_dir / "raw_annotations" / "test_twitter.jsonl", lines=True
    )
    text_snippets, classes, labels = form(
        raw_facebook_df, raw_twitter_df
    )
    pd.testing.assert_frame_equal(
        text_snippets,
        pd.DataFrame(
            {
                "text": [
                    # Facebook
                    "\n\t 1text",
                    "2text",  # This is a duplicated with two classes in raw facebook
                    "ztext",
                    # Twitter
                    "1text",  # This is a duplicated with two classes in raw twitter
                    "text",
                    "ztext",
                ],
                "platform": [
                    "facebook", "facebook", "facebook",
                    "twitter", "twitter", "twitter"
                ],
            },
            index=pd.Index([0, 1, 2, 3, 4, 5], name="id"),
        ),
    )
    assert list(classes.index) == [
        "is_attitude_polarization_accept",
        "is_attitude_polarization_reject",
        "is_christianity_accept",
        "is_christianity_reject",
    ]
    expected_labels = pd.DataFrame(
        {
            "class_name": [
                # Facebook
                "is_christianity_accept",
                "is_christianity_reject",
                "is_attitude_polarization_reject",
                "is_attitude_polarization_reject",
                # Twitter
                "is_attitude_polarization_accept",
                "is_christianity_reject",
                "is_christianity_accept",
                "is_attitude_polarization_reject",
            ],
            "labeled_by": [ANNOTATOR_ID] * 8,
            "labeled_at": [
                pd.Timestamp(1695034512000000000),
                pd.Timestamp(1695034518000000000),
                pd.Timestamp(1695034523000000000),
                pd.Timestamp(1695034521000000000),
                pd.Timestamp(1695033992000000000),
                pd.Timestamp(1695034014000000000),
                pd.Timestamp(1695034002000000000),
                pd.Timestamp(1695033979000000000),
            ],
        },
        index=pd.Index([0, 1, 1, 2, 3, 3, 4, 5], name="text_snippet_id"),
    )
    expected_labels["class_name"] = expected_labels[
        "class_name"
    ].astype("category")
    pd.testing.assert_frame_equal(
        labels,
        expected_labels,
    )


if __name__ == "__main__":
    main()
