# Shared Data - Automatic Classifiers for Peace (ACfP)

This folder contains shared data for the ACfP. The goals is to have a place where organisations and
individuals can make visible data that they are willing to share but that they have full control
over who can use and see the contents of the data. This shared data can then be used by others in
and outside this repo hopefully facilitate the building of classifiers in a cross organisation and
open way.

Currently we think that mostly annotated dataset will be shared but any data that is <100GB could
be shared.

Technically shared data uses [dvc](https://dvc.org/) for the management and versioning of the data
and the idea of isolated dvc projects in a monorepo using
[`--subdir`](https://dvc.org/doc/command-reference/init#initializing-dvc-in-subdirectories) to have
different folders with different remote storages. Be aware that `dvc` can be used for many [use
cases](https://dvc.org/doc/use-cases) however it currently only used in `shared_data` for data
management

## Ownership
A piece of data, or dataset, must have an owner. The owner will have an owner id and all data of
that data owner will be in the folder `/data/<owner_id>/` of this repo. This folder should:
- Have a `README.md` explaining usage, how to access, data protection policy and information about
  the owner
- Have dvc project initialised using: `dvc init --subdir /data/<owner_id>/` with the default remote
  of this dvc subdirectory being a [remote
  storage](https://dvc.org/doc/user-guide/data-management/remote-storage) that is administrated by
  the owner. This means the owner controls who has permission to pull data in this folder.

## Folder structure
The folder's should be structured in a way that is easy to understand. It is thought that data
should be follow this structure:
- `/shared_data/<owner_id>/README.md`: Information about the data owner and usage
- `/shared_data/<owner_id>/<classification_id>/`: A directory containing all the shared data about
- `/shared_data/<owner_id>/<classification_id>/README.md`: information about the classification.
  This is stored in the git version control and it's contents is publicly visible in the
  code/repository.
- `/shared_data/<owner_id>/<classification_id>/annotations.[csv|parquet]`: The annotations for a
  classification. This is stored in the remote storage for that data owner and is not visible via
  the code/repository
- `/shared_data/<owner_id>/<classification_id>/annotations.[csv|parquet].dvc`: The dvc file that
  stores the metadata about the data file that is stored in remote storage. This is visible in the
  code/repository but contains no content information about the data beyond it's name and size.

## Usage
To use data:
- Decide which data you need looking at the folders of the owners and their README.md. Be aware
  that in the code/repository data files will not be visible but the `dvc` metadata
  (`<filename>.<file extention>.dvc`) will be.
- Set up the needed data access following the instructions of the data owners README.md
- Pull the data locally:
  - `cd /shared_data/<owner_id>/`
  - `dvc pull` for all data or `dvc pull <target>` for a subset of the data
- Use the data as you would a local file
- Once you have finished with the data remove it from the local disk, see below

For more advanced data management follow the [`dvc` documentation](https://dvc.org/doc).

To add new data:
- Make sure you have access to put data into the remote data storage of that data owner.
- Make sure that the data owner is in agreement that the data can be added to `shared_data`.
- Add a new folder with `<classification_id>` as the name.
- Add a `README.md` to the folder and commit this in to the git version control. The `README.md`
  should describe the data.
- Add the data into the folder: `cd` into the folder and run `dvc add <file name>` and follow the
  instructions for committing this to git version control.
- Remove the file from the local disk: `dvc push` and `dvc gc --not-in-remote`
- Check your commits so that only the `README.md`, `<file name>.<file extension>.dvc` and addition
  to `.gitignore` are in the history.

## Important data protection measure
Text data that has been taken from public social media messages and the names of accounts that
published the messages fall under GDPR [personal data](https://gdpr.eu/eu-gdpr-personal-data/). As
such this data should be handled with care. See the data protection policies of that data's owner
for specifics on this.

In general, however, data should not be kept on the local machine for longer then is needed. Once
you have finished using data you should clean the data from your local machine while keeping it on
the remote storage. To do this:
- Check that you have no data in the workspace `dvc status` if you do then either commit this data
  or remove it from the workspace
- `dvc gc --num 1` to remove all past data apart from the last commit
- `dvc gc --not-in-remote`  to remove all data that is in remote storage

## Publicly available data
It can be advantageous to split data into public and non public for the same owners. Data that is
public for an owner id should be in the format: `public_<owner_id>` and the remote storage should
have read permissions for anyone.

A use case for this is: the results of an experiment can be made public without exposing (or needing
extra permissions) for personal data (text, author ids). This can be done by storing all personal
data in a "closed" owner folder and having results in a public owner folder. The results would then
reference the personal data but can be viewed and used without the need for the personal data.
